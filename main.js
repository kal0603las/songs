var Vue = require('vue');
// import App from './app.vue';
var VueRouter = require('vue-router');
import addSong from './components/addSong.vue';
Vue.use(VueRouter);

const routes = [
    {
        name: 'add',
        path: '/add',
        component: addSong
    }
];
const router = new VueRouter({ mode: 'history', routes: routes });

Vue.config.productionTip = false;

new Vue({
    render: h => h(App),
    router
}).$mount('#app');