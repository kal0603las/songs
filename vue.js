new Vue({
    el: '#app',
    data: {
        music: {
            "song": null,
            "artist": null,
            "releaseDate": null
        },
        addSong: {
            "song": null,
            "artist": null,
            "releaseDate": null
        },
        allSongs: [],
        updateSong: {
            "id": null,
            "song": null,
            "artist": null,
            "releaseDate": null
        },
        deleteSong: {
            "id": null
        },
        updatemodalSong: {
            "id": null,
            "song": null,
            "artist": null,
            "releaseDate": null
        },
        openModal: {
            "id": null,
            "song": null,
            "artist": null,
            "releaseDate": null
        },
    },
    mounted: function() {
        this.getAllSongs();
    },
    methods: {
        getAllSongs() {
            this.$http.get('http://localhost:2083/index.html')
                .then(function (response) {
                    console.log(response);
                    this.allSongs = response.body;
                });

        },

        insertSong() {
            console.log('test');
            this.$http.post('http://localhost/untitled/index.html', this.addSong)
                .then(function (response) {
                    console.log(response);
                    this.addSong.song = response.body.song;
                    this.addSong.artist = response.body.artist;
                    this.addSong.releaseDate = response.body.releaseDate;
                });

        },

        changeSong() {
            console.log('test');
            this.$http.put('http://localhost/untitled/index.html'+ this.updateSong.id, this.updateSong)
                .then(function (response) {
                    console.log(response);
                    this.updateSong.song = response.body.song;
                    this.updateSong.artist = response.body.artist;
                    this.updateSong.releaseDate = response.body.releaseDate;
                });
        },
        removeSong() {
            this.$http.delete('http://localhost/untitled/index.html'+ this.deleteSong.id)
                .then(function (response) {
                    console.log(response);
                });

        },
        changemodalSong() {
            console.log('test');
            this.$http.put('http://localhost/untitled/index.html'+ this.song.id, this.song)
                .then(function (response) {
                    console.log(response);
                    this.updatemodalSong.song = response.body.song;
                    this.updatemodalSong.artist = response.body.artist;
                    this.updatemodalSong.releaseDate = response.body.releaseDate;
                });
        },
        open_Modal: function (id) {
            var song = this.allSongs.filter(function (song) {
                return song.id === id;
            })[0];
            this.song = song;
            console.log(song);
        },
        removemodalSong() {
            this.$http.delete('http://localhost/untitled/index.html'+ this.song.id)
                .then(function (response) {
                    console.log(response);
                    location.reload();
                });
        }
    }
});
