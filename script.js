var express = require('express');
var mysql = require('mysql');
var router = express.Router();
var app = express();

var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// app.use(require('./routes/index'));


var connect = mysql.createConnection({
    //properties...
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'mymusic'
});


connect.connect(function (error) {
    //callback...
    if(!!error){
        console.log('Error');
    } else {
        console.log('Ühendatud');
    }
});

app.get('/songs', function (req, resp) {
    console.log('hi!');
    // about mysql
    connect.query('SELECT id, song, artist, releaseDate FROM music', function (error, rows, fields) {
        // callback
        if(!!error){
            console.log('Error in the query');
        } else {
            // parse with your rows/fields
            console.log('Successful query');
            resp.send({ error: false, data: rows});
        }
    });
});

app.post('/songs', function (req, resp) {

    let song = req.body;

    if(!song) {
        return resp.status(400).send({error:true, message: 'Please provide song'});
    }
    connect.query('INSERT INTO music SET ? ', song, function(error, rows, fields){
        if(error) throw error;
        return resp.send({error: false, data: rows,  message: 'New song has been created successfully. '});
    });
});

app.delete('/songs:id', function (req, resp){
    let id = req.params.id;

    connect.query('DELETE FROM music WHERE id=?', [id], function (error, rows, fields){
        if(error) throw error;
        return resp.send({error: false, data: rows, message: 'Song has been deleted successfully'});
    });
});

app.put('/songs:id', function(req, resp){
    let id = req.params.id;
    let song = req.body;

    if(!id || !song) {
        return resp.status(400).send({error: song, message: 'Please provide song and id'});
    }

    
    connect.query('UPDATE music SET ? WHERE id = ?', [song, id], function (error, rows, fields){

        return resp.send({error: false, data: rows, message: 'Table has been updated successfully'});
    });

});

const port = 2083;

app.listen(port, () => {
    console.log('Töötan ' + port)
});
